package com.workshop.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Especialista {
	@Id
	@Column(name="id",nullable=false,unique=true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(name="nombreEspecialista",length=200,nullable=false,unique=true)
	private String nombreEspecialista;
	@Column(name="rutEspecialista",length=200,nullable=false,unique=true)
	private String rutEspecialista;
	@Column(name="emailEspecialista",length=100,nullable=false,unique=true)
	private String emailEspecialista;
	
	public String getEmailEspecialista() {
		return emailEspecialista;
	}
	public void setEmailEspecialista(String emailEspecialista) {
		this.emailEspecialista = emailEspecialista;
	}
	public String getNombreEspecialista() {
		return nombreEspecialista;
	}
	public void setNombreEspecialista(String nombreEspecialista) {
		this.nombreEspecialista = nombreEspecialista;
	}
	public String getRutEspecialista() {
		return rutEspecialista;
	}
	public void setRutEspecialista(String rutEspecialista) {
		this.rutEspecialista = rutEspecialista;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}
