package com.workshop.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workshop.models.EspecialidadMedica;
@Repository
public interface IEspecialidadMedicaDAO extends JpaRepository <EspecialidadMedica, Integer> {

}
