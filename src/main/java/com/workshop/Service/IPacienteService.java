package com.workshop.Service;

import java.util.List;

import com.workshop.models.Paciente;


public interface IPacienteService {

	Paciente persist(Paciente e);
	List<Paciente> getAll();
	Paciente findBYId(Integer id);
	Paciente merge(Paciente e);
	void delete(Integer id);
}
