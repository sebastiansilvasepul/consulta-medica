package com.workshop.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workshop.DAO.IEspecialistaDAO;
import com.workshop.Service.IEspecialistaService;
import com.workshop.models.Especialista;
@Service
public class EspecialidadService implements IEspecialistaService {

	@Autowired 
	IEspecialistaDAO service;
	@Override
	public Especialista persist(Especialista e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Especialista> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Especialista findBYId(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Especialista merge(Especialista e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		 service.delete(id);
	}

}
